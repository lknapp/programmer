--entities
local Player = require("src.entities.Player")
local Tracker = require("src.entities.Tracker")
local Waves = require("src.entities.Waves")
local Bouncers = require("src.entities.Bouncers")

--behaviors
local PlayerBehavior = require("src.behaviors.Player")
local Control = require("src.player.Control")
local TrackerBehavior = require("src.behaviors.Trackers")

--interactions
local BouncersTrackers = require("src.interactions.BouncersTrackers")
local PlayerBouncers = require("src.interactions.PlayerBouncers")

--rendering
local RenderPlayer = require("src.player.Render")
local Scene = require("src.render.Scene")
local Effects = require("src.render.Effects")

--game
local Level = require("src.Level")

local data = {
  player = {},
  waves = {},
  trackers = {},
  effects = {},
  bouncers = {},
}


function love.load()
  love.graphics.setLineWidth(5)
  love.window.setMode(600, 800, {fullscreen = false, resizable = true, x = 1000, y = 0} )
  data.player = Player.new()
  data.waves = Waves.initialize()
  data.bouncers = Bouncers.initialize()
end

function love.update(dt)
  data.trackers = Tracker.spawn(data.trackers)
  data.player = Control(data.player, dt, love.keyboard)
  data.player = PlayerBehavior.move(data.player, dt)
  data.trackers = TrackerBehavior.move(data, dt)
  data.bouncers = Bouncers.move(data.player, data.bouncers, dt)
  data.player = Level.enforceBoundaries(data.player)
  data = PlayerBehavior.detectWaves(data)
  data = PlayerBehavior.detectEnemyCollisions(data)
  data = BouncersTrackers.interact(data)
  data = PlayerBouncers.detectBouncers(data)
  data.effects = Effects.update(data.effects, dt)
  data.waves = Waves.spawn(data.waves)
end

function love.draw()
  Scene.renderBackground()
  Effects.render(data.effects)
  Scene.render(data)
  RenderPlayer(data.player)
  Waves.render(data.waves)
  Bouncers.render(data.bouncers, data.player)
  Tracker.render(data.trackers)
end

