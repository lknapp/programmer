local TrackerBehaviors = {}

function TrackerBehaviors.move(data, dt)
  trackers = data.trackers
  for i, tracker in ipairs(trackers) do
    xdist = data.player.x - tracker.x
    ydist = data.player.y - tracker.y
    totalDist = (ydist^2 + xdist^2)^(1/2)
    xspeed = tracker.speed * xdist / totalDist
    yspeed = tracker.speed * ydist / totalDist
    tracker.x = tracker.x + xspeed*dt
    tracker.y = tracker.y + yspeed*dt
  end
  return trackers
end

return TrackerBehaviors


