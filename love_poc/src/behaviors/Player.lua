local Tracker = require("src.entities.Tracker")
local Waves = require("src.entities.Waves")

local Sound = require("src.render.Sound")
local Geo = require("src.science.Geometry")
local Physics = require("src.science.Physics")
local Behavior = {}

function Behavior.move(player, dt)
  target = {
    x = player.targetx,
    y = player.targety,
  }
  player = Physics.dampenSpeed(player, 0.98)
  player = Physics.accelerateToward(player, target, 10, dt)
  return player
end

function Behavior.explodeTrackers(data)
   if (next(data.waves) == nil and (not (next(data.trackers) == nil))) then
     for j, enemy in ipairs(data.trackers) do
       table.insert(data.effects, {
         name = "tracker-explosion",
         x = enemy.x,
         y = enemy.y,
         radius = 5
       })
     end
     data.trackers = {}
     Sound.ping()
   end
   return data
end

function Behavior.detectWaves(data)
  player = data.player
  for i, wave in ipairs(data.waves) do
    if Geo.collide(player, wave) then
      Sound.randomChime()
      data.effects = Waves.registerEffect(wave, data.effects)
      table.remove(data.waves, i)
      data = Behavior.explodeTrackers(data)
    end
  end
  return data
end

function Behavior.detectEnemyCollisions(data)
  for i, enemy in ipairs(data.trackers) do
    xdist = math.abs(data.player.x - enemy.x)
    ydist = math.abs(data.player.y - enemy.y)
    hypotenuse = data.player.radius + enemy.radius
    if (math.pow(xdist, 2) + math.pow(ydist, 2) < math.pow(hypotenuse, 2)) then
      Sound.bonk()
      data.player = Tracker.actuate(enemy, data.player)
      table.insert(data.effects, {
          name = "tracker-effect",
          radius = 5,
          x = enemy.x,
          y = enemy.y,
          strength = 2
        })
      table.remove(data.trackers, i)
    end
  end
  return data
end

return Behavior
