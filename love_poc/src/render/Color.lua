local Colors = require("src.render.Colors")

function Color(name, a)
  color = Colors[name]
  r = color[1]
  g = color[2]
  b = color[3]
  a = a or color[4] or 1
  love.graphics.setColor(r/255, g/255, b/255, a)
end

return Color
