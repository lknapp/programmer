local Sound = {}

local masterVolume = 0.0

local function source(path, mode)
  s = love.audio.newSource(path, mode)
  s:setVolume(masterVolume)
  return s
end

function Sound.randomChime()
  chime = source("assets/ding.wav", "static")
  randomChime = love.math.random()
  if randomChime > 0 and randomChime < 0.2 then
    chime:setPitch(1/2)
  end
  if randomChime > 0.2 and randomChime < 0.4 then
    chime:setPitch(2/3)
  end
  if randomChime > 0.4 and randomChime < 0.6 then
    chime:setPitch(3/4)
  end
  if randomChime > 0.6 and randomChime < 0.8 then
    chime:setPitch(8/9)
  end
  chime:play()
end

function Sound.bonk()
  source("assets/bonk.wav", "static"):play()
end

function Sound.buzz()
  source("assets/buzz.wav", "static"):play()
end

function Sound.ping()
  ping = source("assets/ping.flac", "static")
  ping:setPitch(2)
  ping:play()
end

return Sound;
