local config = require("src.config")
local Scene = {}

function Scene.render(data)
  Color("frame")
  love.graphics.rectangle("line", config.minx, config.miny, config.maxx, config.maxy)
end

function Scene.renderBackground()
  Color("background")
  love.graphics.rectangle("fill", 0, 0, 1000, 1000)
end

return Scene;
