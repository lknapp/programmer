local Effects = {}


function Effects.update(effects, dt)
  for i, effect in ipairs(effects) do
    if effect.name == "tracker-effect" then
      effect.radius = effect.radius + dt * 200
      if effect.radius > 2000 then
        table.remove(effects, i)
      end
    end
    if effect.name == "wave" then
      effect.radius = effect.radius + dt * 80
      effect.rotation = effect.rotation + dt*effect.spin
      if effect.radius > 2000 then
        table.remove(effects, i)
      end
    end
    if effect.name == "tracker-explosion" then
      effect.radius = effect.radius + dt * 100
      if effect.radius > 2000 then
        table.remove(effects, i)
      end
    end
    if effect.name == "tracker-fizzle" then
      effect.radius = effect.radius - dt * 20
      if effect.radius < 0 then
        table.remove(effects, i)
      end
    end
  end
  return effects
end

function Effects.render(effects)
  for i, effect in ipairs(effects) do
    if effect.name == "tracker-effect" then
      Color("grey")
      love.graphics.circle("fill", effect.x, effect.y, effect.radius, 4)
    end
    if effect.name == "wave" then
      love.graphics.translate(effect.x, effect.y)
      love.graphics.rotate(effect.rotation)
      if effect.sides == 2 then
        Color(effect.color)
        love.graphics.circle("fill", 0, 0, effect.radius, 20)
        Color("white")
        love.graphics.circle("line", 0, 0, effect.radius, 20)
      end
      Color(effect.color)
      love.graphics.circle("fill", 0, 0, effect.radius, effect.sides)
      Color("white")
      love.graphics.circle("line", 0, 0, effect.radius, effect.sides)
      love.graphics.origin()
    end
    if effect.name == "tracker-explosion" then
      love.graphics.translate(effect.x, effect.y)
      Color("white")
      love.graphics.circle("line", 0, 0, effect.radius, 3)
      love.graphics.origin()
    end
    if effect.name == "tracker-fizzle" then
      Color("white")
      love.graphics.circle("line", effect.x, effect.y, effect.radius, 3)
    end
  end
end

return Effects
