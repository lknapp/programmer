return {
  background =  {100,   100,  100},
  frame =       {0, 0, 0},
  trackerPop=   {255, 0, 0},
  red=          {255, 0, 0},
  white=        {255, 255, 255},
  grey=         {155, 155, 155},

  --blue=         {232, 94, 255},
  --green=        {243, 255, 220},
  --chartreuse=   {197, 255, 102},
  --goldenrod=    {255, 203, 180},
  --orange=       {117, 207, 255},

  blue=         {76, 97, 51},
  green=        {237, 190, 114},
  chartreuse=   {84, 45, 58},
  goldenrod=    {51, 52, 107},
  orange=       {47, 97, 79},

  target=       {0, 0, 0},
  wall=       {255, 255, 255},
  text=       {255, 255, 255},
  black=       {0, 0, 0},
}
