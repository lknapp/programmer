local Geo = require("src.science.Geometry")
local PB = {}

function PB.detectBouncers(data)
  player = data.player
  for i, bouncer in ipairs(data.bouncers) do
    if Geo.collide(player, bouncer) then
      bouncer.discovered = true
    end
  end

  bothDiscovered = true

  for i, bouncer in ipairs(data.bouncers) do
    if (not(bouncer.discovered) and not(bouncer.tethered)) then
      bothDiscovered = false
    end
  end

  if bothDiscovered
     and Geo.lineCollide(player, data.bouncers[1], data.bouncers[2])
     and Geo.obtuse(data.bouncers[1], player, data.bouncers[2])
     and not Geo.collide(player, data.bouncers[1])
     and not Geo.collide(player, data.bouncers[2])
       then
    for i, bouncer in ipairs(data.bouncers) do
      bothDiscovered = false
      bouncer.discovered = false
      bouncer.tethered = false
    end
  end

  if bothDiscovered then
    for i, bouncer in ipairs(data.bouncers) do
      bouncer.discovered = false
      bouncer.tethered = true
    end
  end

  return data
end


return PB
