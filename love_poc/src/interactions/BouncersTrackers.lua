local BouncersTrackers = {}
local Geo = require("src.science.Geometry")
local Sound = require("src.render.Sound")

function BouncersTrackers.interact(data)
  player = data.player
  bouncer1 = data.bouncers[1]
  bouncer2 = data.bouncers[2]
  trackers = data.trackers

  if bouncer1.tethered then
    for j, tracker in ipairs(trackers) do
      if Geo.lineCollide(tracker, bouncer1, bouncer2) and
         Geo.obtuse(bouncer1, tracker, bouncer2) then
        table.remove(data.trackers, j)
        table.insert(data.effects, {
          name = "tracker-fizzle",
          x = tracker.x,
          y = tracker.y,
          radius = 10,
        })
        Sound.buzz()
      end
    end
  end

  return data
end

return BouncersTrackers
