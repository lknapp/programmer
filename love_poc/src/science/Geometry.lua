local Geo = {}

function Geo.collide(a, b)
    xdist = math.abs(a.x - b.x)
    ydist = math.abs(a.y - b.y)
    hypotenuse = a.radius + b.radius
    return (math.pow(xdist, 2) + math.pow(ydist, 2) < math.pow(hypotenuse, 2))
end

function Geo.lineCollide(circle, a, b)
  dist =
    ((b.y - a.y)*circle.x - (b.x - a.x)*circle.y + b.x*a.y - b.y*a.x) /
    ((a.y - b.y)^2 + (a.x - b.x)^2)^(1/2)
  return math.abs(dist) < circle.radius
end

function Geo.obtuse(a, b, c)
  ab = {
    x = a.x - b.x,
    y = a.y - b.y,
  }
  bc = {
    x = c.x - b.x,
    y = c.y - b.y,
  }
  lenAb = ((a.x - b.x)^2 + (a.y - b.y)^2)^(1/2)
  lenBc = ((b.x - c.x)^2 + (b.y - c.y)^2)^(1/2)
  dotProduct = ab.x * bc.x + ab.y * bc.y

  angle = math.acos(dotProduct / (lenAb * lenBc))

  return (angle > math.pi / 2) and (angle < math.pi * 3/2)
end

return Geo
