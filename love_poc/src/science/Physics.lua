local Physics = {}

function Physics.accelerateToward(subject, attractor, acceleration, dt)
  totalDistance = ((subject.x - attractor.x)^2 + (subject.y- attractor.y)^2)^(1/2)
  xdist = attractor.x - subject.x
  ydist = attractor.y - subject.y
  yaccel = acceleration * ydist / totalDistance
  xaccel = acceleration * xdist / totalDistance
  if totalDistance == 0 then
    xaccel = 0
    yaccel = 0
  end
  subject.xspeed = subject.xspeed + xaccel*dt
  subject.yspeed = subject.yspeed + yaccel*dt
  subject.x = subject.x + subject.xspeed
  subject.y = subject.y + subject.yspeed
  return subject
end

function Physics.dampenSpeed(subject, dampeningCoefficient)
  subject.xspeed = subject.xspeed * dampeningCoefficient
  subject.yspeed = subject.yspeed * dampeningCoefficient
  return subject
end

return Physics
