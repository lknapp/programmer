local Player = {}

function Player.new()
  return {
    radius = 15,
    x = 50,
    y = 50,
    trackers = 0,
    level = 3,
    targetx = 50,
    targety = 50,
    xspeed = 1,
    yspeed = 1,
    targetspeed=800,
  }
end

return Player
