local Color = require("src.render.Color")
local config = require("src.config")
local Tracker = {}

function Tracker.new(x, y)
  return {
    name = "tracker",
    x = x,
    y = y,
    speed = 200,
    radius = 8
  }
end

function Tracker.render(trackers)
  for i, tracker in ipairs(trackers) do
    Color("white")
    love.graphics.circle("fill", tracker.x, tracker.y, tracker.radius, 3)
    Color("black")
    love.graphics.circle("line", tracker.x, tracker.y, tracker.radius + 3, 3)
  end
end


function Tracker.actuate(tracker, player)
  player.trackers = player.trackers + 1
  return player
end

function Tracker.spawn(trackers)
  if math.random() < 0.005 then
    table.insert(trackers, Tracker.new(
      love.math.random(config.minx, config.maxx),
      love.math.random(config.miny, config.maxy)
    ))
  end
  return trackers
end

return Tracker
