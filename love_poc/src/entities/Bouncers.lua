local Bouncers = {}
local Color = require("src.render.Color")
local config = require("src.config")
local Physics = require("src.science.Physics")

function Bouncers.initialize()
  bouncers = {}
  table.insert(bouncers, {
    name = "bouncer",
    x= math.random(config.minx, config.maxx),
    y = math.random(config.miny, config.maxy),
    xspeed = math.random()*5,
    yspeed = math.random()*5,
    tethered = false,
    discovered = false,
    radius = 10,
  })
  table.insert(bouncers, {
    name = "bouncer",
    x= math.random(config.minx, config.maxx),
    y = math.random(config.miny, config.maxy),
    xspeed = math.random()*5,
    yspeed = math.random()*5,
    tethered = false,
    discovered = false,
    radius = 10,
  })
  return bouncers
end

function Bouncers.move(player, bouncers, dt)
  for i, bouncer in ipairs(bouncers) do
    if bouncer.discovered or bouncer.tethered then
      bouncer.x = bouncer.x + bouncer.xspeed
      bouncer.y = bouncer.y + bouncer.yspeed
    end
    if bouncer.x > config.maxx or bouncer.x < config.minx then
      bouncer.xspeed = bouncer.xspeed * -1
    end
    if bouncer.y > config.maxy or bouncer.y < config.miny then
      bouncer.yspeed = bouncer.yspeed * -1
    end
  end
  return bouncers
end

function Bouncers.render(bouncers, player)
  bouncer1 = bouncers[1]
  bouncer2 = bouncers[2]

  for i, bouncer in ipairs(bouncers) do
    if bouncer.discovered then
      Color("white")
      love.graphics.line(player.x, player.y, bouncer.x, bouncer.y)
    end
    Color("white")
    love.graphics.circle("fill", bouncer.x, bouncer.y, bouncer.radius, 30)
  end

  if bouncer1.tethered then
    Color("white")
    love.graphics.line(bouncer1.x, bouncer1.y, bouncer2.x, bouncer2.y)
  end

end

return Bouncers

