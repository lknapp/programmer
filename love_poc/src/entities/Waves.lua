local Waves = {}
local Color = require("src.render.Color")
local config = require("src.config")

function Waves.spawn(waves)
  if next(waves) == nil then
    waves = Waves.initialize()
  end
  return waves
end

function Waves.initialize()
  waves = {}
  table.insert(waves, Waves.new("blue", math.random(2, 7)))
  table.insert(waves, Waves.new("green", math.random(2, 7)))
  table.insert(waves, Waves.new("chartreuse", math.random(2, 7)))
  table.insert(waves, Waves.new("goldenrod", math.random(2, 7)))
  table.insert(waves, Waves.new("orange", math.random(2, 7)))
  return waves
end

function Waves.new(color, sides)
  return {
    name = "wave",
    spin = math.random()*4-2,
    color = color,
    sides = sides,
    x= math.random(config.minx, config.maxx),
    y = math.random(config.miny, config.maxy),
    radius = 10
  }
end

function Waves.render(waves)
  for i, wave in ipairs(waves) do
    if wave.sides == 2 then
      Color("white")
      love.graphics.circle("line", wave.x, wave.y, 13, 100)
      Color(wave.color)
      love.graphics.circle("fill", wave.x, wave.y, 10, 100)
    else
      Color("white")
      love.graphics.circle("line", wave.x, wave.y, 13, wave.sides)
      Color(wave.color)
      love.graphics.circle("fill", wave.x, wave.y, 10, wave.sides)
    end
  end
end

function Waves.registerEffect(wave, effects)
   table.insert(effects, {
       name = "wave",
       rotation= 0,
       spin = wave.spin,
       sides = wave.sides,
       color = wave.color,
       x = wave.x,
       y = wave.y,
       radius = 5
     })
   return effects
end

return Waves

