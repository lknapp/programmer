local Color = require("src.render.Color")

local function Render(player)
  Color("white")
  love.graphics.line(player.x, player.y, player.targetx, player.targety)
  love.graphics.circle("fill", player.x, player.y, player.radius, 10)
  Color("white")
  love.graphics.circle("fill", player.x, player.y, player.radius - 5, 10)
  Color("target")
  love.graphics.circle("line", player.targetx, player.targety, 3, 10)
end

return Render

