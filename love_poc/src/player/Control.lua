local function Control(player, dt, keyboard)
  if keyboard.isDown("l") then
    player.targetx = player.targetx + player.targetspeed * dt
  end
  if keyboard.isDown("j") then
    player.targetx = player.targetx - player.targetspeed * dt
  end
  if keyboard.isDown("i") then
    player.targety = player.targety - player.targetspeed * dt
  end
  if keyboard.isDown("k") then
    player.targety = player.targety + player.targetspeed * dt
  end
  return player
end

return Control
