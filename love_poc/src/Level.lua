local config = require("src.config")
local Tracker = require("src.entities.Tracker")
local Level = {}

function Level.enforceBoundaries(player)
  if player.targety > config.maxy then
    player.targety = config.maxy
  end

  if player.targety < config.miny then
    player.targety = config.miny
  end

  if player.targetx > config.maxx then
    player.targetx = config.maxx
  end

  if player.targetx < config.minx then
    player.targetx = config.minx
  end
  return player
end

return Level
