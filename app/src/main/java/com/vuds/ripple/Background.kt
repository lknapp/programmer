package com.vuds.ripple

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import com.vuds.ripple.utils.ColorUtil

object Background {
    var backgroundColor = Color.DKGRAY

    fun update() {
        backgroundColor = ColorUtil.darken(backgroundColor)
    }

    fun draw(canvas: Canvas?) {
        val paint =
            Paint().apply {
                isAntiAlias = true
                color = backgroundColor
                style = Paint.Style.FILL_AND_STROKE
            }

        val rect = Rect(0, 0, Resources.getSystem().getDisplayMetrics().widthPixels, Resources.getSystem().getDisplayMetrics().heightPixels)
        canvas?.drawRect(rect, paint)
    }
}