package com.vuds.ripple

import android.animation.TimeAnimator
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.vuds.ripple.entities.Cursor
import com.vuds.ripple.entities.EntityManager
import com.vuds.ripple.entities.Tracker
import com.vuds.ripple.utils.ScreenUtil

class CanvasView @JvmOverloads constructor(context: Context,
                                           attrs: AttributeSet? = null, defStyleAttr: Int = 0)
: View(context, attrs, defStyleAttr) {

    private val cursor: Cursor = Cursor(ScreenUtil.centerX(), 70.0)
    private val tracker: Tracker = Tracker(ScreenUtil.centerX(), 70.0, cursor)
    private val entityManager: EntityManager = EntityManager(tracker)

    override fun onFinishInflate() {
        animateProgress()
        super.onFinishInflate()
    }

    override fun onDraw(canvas: Canvas?) {
        Background.draw(canvas)

        entityManager.drawBackground(canvas)

        cursor.draw(canvas)
        tracker.draw(canvas)
        entityManager.drawForeground(canvas)

        super.onDraw(canvas)
    }

    fun animateProgress() {
        val animator = TimeAnimator()
        animator.setTimeListener { _, _, dt ->
            tracker.update(dt)
            Background.update()
            entityManager.update(dt)
            invalidate()
        }
        animator.start()
    }

    override fun onTouchEvent(e: MotionEvent): Boolean {
        cursor.moveTo(e.x.toDouble(), e.y.toDouble())
        invalidate()
        return true
    }
}