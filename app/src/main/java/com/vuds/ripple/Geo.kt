package com.vuds.ripple

import com.vuds.ripple.entities.Entity
import java.lang.Math.abs
import kotlin.math.pow

class Geo {
    companion object {
        fun detectCollision(a: Entity, b: Entity): Boolean {
            var xDist = abs(a.x - b.x)
            var yDist = abs(a.y - b.y)
            var hypotenuse = a.r + b.r
            return (xDist.pow(2) + yDist.pow(2)) < hypotenuse.pow(2)
        }
    }
}