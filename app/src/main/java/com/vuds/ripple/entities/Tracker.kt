package com.vuds.ripple.entities

import android.graphics.Canvas
import android.graphics.Paint
import com.vuds.ripple.utils.ColorUtil
import kotlin.math.pow
import kotlin.math.sqrt

class Tracker(x: Double, y:Double, cursor: Cursor): Entity(x, y, 60.0) {
    private val cursor = cursor
    private val tail = mutableListOf<TailElement>()
    private var length = 1
    private var dx = 0.0
    private var dy = 0.0
    private var ddx = 0.0
    private var ddy = 0.0
    private var acceleration = 0.05
    private var frictionCoefficient = 0.99
    var c = ColorUtil.randomLightColor()

    private class TailElement(x:Double, y:Double, c:Int) {
        var x = x
        var y = y
        var c = c
    }

    fun heading(): Double {
        return Math.toDegrees(Math.atan((dy / dx)))
    }

    fun rotationRate(): Double {
        var theta = Math.atan(dx / dy)
        var thetaPrime = Math.atan((2*dx + ddx) / (2*dy + ddy))
        return Math.toDegrees(thetaPrime -  theta)
    }


    override fun update(dt: Long) {
        accelerate(dt)
        applyFriction()
        c = ColorUtil.rotateHue(c)
        tail.add(TailElement(x, y, c))
        if (tail.size > length) {
            tail.removeAt(0)
        }
    }

    private fun accelerate(dt: Long) {
        val xDist = (cursor.x - x)
        val yDist = (cursor.y - y)
        val totalDist = sqrt(xDist.pow(2) + yDist.pow(2))
        if(totalDist != 0.0) {
            ddx = acceleration * xDist * dt / totalDist
            ddy = acceleration * yDist * dt / totalDist
        } else {
            ddx = 0.0
            ddy = 0.0
        }

        dx += ddx
        dy += ddy
        x += dx
        y += dy
    }

    private fun applyFriction() {
        dx *= frictionCoefficient
        dy *= frictionCoefficient
    }

    fun grow() {
        length += 1
    }

    override fun draw(canvas: Canvas?) {
        tail.forEachIndexed { i, t ->
            val p =
                Paint().apply {
                    isAntiAlias = true
                    color = t.c
                    style = Paint.Style.FILL_AND_STROKE
                }

            canvas?.drawCircle(t.x.toFloat(), t.y.toFloat(), (i.toFloat()*r/length).toFloat(), p)
        }
        val paint =
            Paint().apply {
                isAntiAlias = true
                color = c
                style = Paint.Style.FILL_AND_STROKE
            }

        canvas?.drawCircle(x.toFloat(), y.toFloat(), r.toFloat(), paint)
    }
}