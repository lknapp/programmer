package com.vuds.ripple.entities

abstract class Item(x: Double, y: Double, r: Double): Entity(x, y, r) {
    abstract fun activate(tracker: Tracker): Entity
}
