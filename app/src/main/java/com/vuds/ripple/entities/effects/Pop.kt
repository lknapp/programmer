package com.vuds.ripple.entities.effects

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Paint
import com.vuds.ripple.entities.Entity
    import com.vuds.ripple.utils.ColorUtil

class Pop(x: Double, y: Double, c: Int): Entity(x, y, 50.0) {
    var growthRate = 0.2f
    var c = c

    override fun draw(canvas: Canvas?) {
        var paint = Paint().apply {
            isAntiAlias = true
            color = c
            style = Paint.Style.FILL_AND_STROKE
        }
        canvas?.drawCircle(x.toFloat(), y.toFloat(), r.toFloat(), paint)
    }

    override fun update(dt : Long) {
        r += dt*growthRate
        c = ColorUtil.rotateHue(c)
        c = ColorUtil.fade(c)
        if(r > Resources.getSystem().displayMetrics.heightPixels) {
            expired = true
        }
    }
}