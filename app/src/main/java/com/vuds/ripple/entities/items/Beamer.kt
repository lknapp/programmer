package com.vuds.ripple.entities.items

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import com.vuds.ripple.entities.Entity
import com.vuds.ripple.entities.Item
import com.vuds.ripple.entities.Tracker
import com.vuds.ripple.entities.effects.Beam

class Beamer(x: Double, y: Double, c: Int): Item(x, y, 35.0) {
    var c = c
    var paint = Paint().apply {
        isAntiAlias = true
        color = c
        style = Paint.Style.FILL_AND_STROKE
    }
    override fun draw(canvas: Canvas?) {
        canvas?.drawRoundRect(RectF((x-r).toFloat(), (y-r).toFloat(), (x+r).toFloat(), (y+r).toFloat()), 10f, 10f, paint)
    }

    override fun activate(tracker: Tracker): Entity {
        return Beam (x, y, c, tracker.heading(), -tracker.rotationRate()/10.0)
    }

    override fun update(dt: Long){
    }
}