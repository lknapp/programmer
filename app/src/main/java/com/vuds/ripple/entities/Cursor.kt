package com.vuds.ripple.entities

import android.graphics.Canvas

class Cursor(x: Double, y:Double): Entity(x, y, 30.0) {
    fun moveTo(xIn: Double, yIn: Double) {
        x = xIn
        y = yIn
    }

    override fun draw(canvas: Canvas?) {
    }

    override fun update(dt: Long) {
    }

}