package com.vuds.ripple.entities

import android.graphics.Canvas

abstract class Entity(x: Double, y: Double, r: Double) {
    abstract fun update(dt: Long)
    abstract fun draw(canvas: Canvas?)

    var x = x
    var y = y
    var r = r
    var expired = false
}
