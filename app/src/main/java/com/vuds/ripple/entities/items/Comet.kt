package com.vuds.ripple.entities.items

import android.graphics.Canvas
import android.graphics.Paint
import com.vuds.ripple.entities.Entity
import com.vuds.ripple.entities.Item
import com.vuds.ripple.entities.Tracker
import com.vuds.ripple.entities.effects.OscillatingRipple
import com.vuds.ripple.utils.ScreenUtil

class Comet(x: Double, y: Double, c: Int): Item(x, y, 50.0) {
    var c = c
    var dx: Double = Math.random() - 0.5f
    var dy: Double = Math.random() - 0.5f

    var paint = Paint().apply {
        isAntiAlias = true
        color = c
        style = Paint.Style.STROKE
        strokeWidth = 10.0f

    }
    override fun draw(canvas: Canvas?) {
        canvas?.drawCircle(x.toFloat(), y.toFloat(), r.toFloat(), paint)
    }

    override fun activate(tracker: Tracker): Entity {
        return OscillatingRipple(x, y, c, speed(), 50f)
    }

    override fun update(dt: Long) {
        x += dx*dt
        y += dy*dt
        if(x > (ScreenUtil.maxX() + 2*r)) {
            x = 0.0
        }
        if(y > (ScreenUtil.maxY() + 2*r)) {
            y = 0.0
        }
        if(x < - 2*r) {
            x = ScreenUtil.maxX()
        }
        if(y < - 2*r) {
            y = ScreenUtil.maxY()
        }
    }

    private fun speed(): Double {
        return Math.sqrt(Math.pow(dx, 2.0) + Math.pow(dy, 2.0))
    }
}