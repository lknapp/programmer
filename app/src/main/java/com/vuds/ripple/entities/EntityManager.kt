package com.vuds.ripple.entities

import android.graphics.Canvas
import com.vuds.ripple.Geo
import com.vuds.ripple.Level
import com.vuds.ripple.entities.items.Beamer
import com.vuds.ripple.entities.items.Comet
import com.vuds.ripple.entities.items.Popper
import com.vuds.ripple.entities.items.Rippler
import com.vuds.ripple.entities.effects.Splash
import com.vuds.ripple.utils.ColorUtil
import com.vuds.ripple.utils.ScreenUtil
import java.lang.Math.*

class EntityManager constructor(tracker: Tracker) {
    val tracker = tracker
    private var foreground = spawnInitialItems()
    private var background = emptyList<Entity>()


    fun drawForeground(canvas: Canvas?) {
        foreground.forEach { it.draw(canvas) }
    }

    fun drawBackground(canvas: Canvas?) {
        background.forEach { it.draw(canvas) }
    }

    fun update(dt: Long) {
        var updatedForeground = emptyList<Item>()
        foreground.forEach{ it.update(dt) }
        foreground = foreground.filter{ !it.expired }
        foreground.forEach {
            if (Geo.detectCollision(it, tracker)) {
                background += it.activate(tracker)
                tracker.grow()
            } else {
                updatedForeground += it
            }
        }
        foreground = updatedForeground
        foreground = spawnNew(foreground)
        background.forEach{ it.update(dt) }
        background = background.filter{ !it.expired }

    }

    private fun spawnNew(foreground: List<Item>): List<Item> {
        if (foreground.isEmpty()) {
            Level.level += 1
            background = listOf(Splash(tracker.x, tracker.y)) + background
            return spawnInitialItems()
        }
        return foreground
    }

    private fun spawnInitialItems(): List<Item> {
        var items: MutableList<Item> = mutableListOf<Item>()
        while (items.size < Level.level) {
            items.add(spawnItem())
        }
        return items
    }

    private fun spawnItem(): Item {
        if (floor(random() * 4).toInt() == 0) {
            return spawnRippler()
        }
        if (floor(random() * 4).toInt() == 1) {
            return spawnPopper()
        }
        if (floor(random() * 4).toInt() == 2) {
            return spawnComet()
        }
        return spawnBeamer()
    }

    private fun spawnRippler(): Item {
        var x = ScreenUtil.randomPaddedX(35.0)
        var y = ScreenUtil.randomPaddedY(35.0)
        var c = ColorUtil.randomBrightColor()
        return Rippler(x, y, c)
    }

    private fun spawnComet(): Item {
        var x = ScreenUtil.randomPaddedX(35.0)
        var y = ScreenUtil.randomPaddedY(35.0)
        var c = ColorUtil.randomBrightColor()
        return Comet(x, y, c)
    }

    private fun spawnBeamer(): Item {
        var x = ScreenUtil.randomPaddedX(35.0)
        var y = ScreenUtil.randomPaddedY(35.0)
        var c = ColorUtil.randomBrightColor()
        return Beamer(x, y, c)
    }

    private fun spawnPopper(): Item {
        var x = ScreenUtil.randomPaddedX(35.0)
        var y = ScreenUtil.randomPaddedY(35.0)
        var c = ColorUtil.randomBrightColor()
        return Popper(x, y, c)
    }
}