package com.vuds.ripple.entities.effects

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Paint
import com.vuds.ripple.entities.Entity
import com.vuds.ripple.utils.ColorUtil

class OscillatingRipple(x: Double, y: Double, c: Int, growthRate: Double, thickness: Float): Entity(x, y, 50.0) {
    var growthRate = growthRate
    var c = c
    var thickness = thickness

    override fun draw(canvas: Canvas?) {
        var paint = Paint().apply {
            isAntiAlias = true
            color = c
            style = Paint.Style.STROKE
            strokeWidth = (-Math.cos((r - 50)/100)*thickness + 2*thickness).toFloat()
        }
        canvas?.drawCircle(x.toFloat(), y.toFloat(), r.toFloat(), paint)
    }

    override fun update(dt: Long) {
        r += dt*growthRate
        c = ColorUtil.rotateHue(c)
        if(r > Resources.getSystem().displayMetrics.heightPixels) {
            expired = true
        }
    }
}