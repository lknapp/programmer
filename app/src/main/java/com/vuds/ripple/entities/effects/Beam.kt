package com.vuds.ripple.entities.effects

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import com.vuds.ripple.entities.Entity
import com.vuds.ripple.utils.ColorUtil

class Beam(x: Double, y: Double, c: Int, rotation: Double, rotationRate: Double): Entity(x, y, 50.0) {
    var rotation = rotation
    var growthRate =1f
    var rotationRate = rotationRate
    var c = c

    override fun draw(canvas: Canvas?) {
        var paint = Paint().apply {
            isAntiAlias = true
            color = c
            style = Paint.Style.FILL_AND_STROKE
        }
        canvas?.save()
        canvas?.rotate(rotation.toFloat(), x.toFloat(), y.toFloat())
        canvas?.drawRoundRect(RectF((x-r).toFloat(), (y-15).toFloat(), (x+r).toFloat(), (y+15).toFloat()), 10f, 10f, paint)
        canvas?.restore()
    }

    override fun update(dt: Long) {
        r += dt*growthRate
        c = ColorUtil.rotateHue(c)
        rotation += dt*rotationRate
        if(r > Resources.getSystem().displayMetrics.heightPixels) {
            growthRate *= -1
            r = Resources.getSystem().displayMetrics.heightPixels - 1.0
        }
        if(r < 0) {
            expired = true
        }
    }
}