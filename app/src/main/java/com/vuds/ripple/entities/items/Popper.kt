package com.vuds.ripple.entities.items

import android.graphics.Canvas
import android.graphics.Paint
import com.vuds.ripple.entities.Entity
import com.vuds.ripple.entities.Item
import com.vuds.ripple.entities.Tracker
import com.vuds.ripple.entities.effects.Pop

class Popper(x: Double, y: Double, c: Int): Item(x, y, 35.0) {
    var c = c
    var paint = Paint().apply {
        isAntiAlias = true
        color = c
        style = Paint.Style.FILL_AND_STROKE
    }
    override fun draw(canvas: Canvas?) {
        canvas?.drawCircle(x.toFloat(), y.toFloat(), r.toFloat(), paint)
    }

    override fun activate(tracker: Tracker): Entity {
        return Pop(x, y, c)
    }

    override fun update(dt: Long){
    }
}