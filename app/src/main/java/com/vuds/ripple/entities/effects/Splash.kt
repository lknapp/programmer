package com.vuds.ripple.entities.effects

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.vuds.ripple.Background
import com.vuds.ripple.entities.Entity

class Splash(x: Double, y: Double): Entity(x, y, 0.0) {
    var growthRate = 3f

    override fun draw(canvas: Canvas?) {
        var paint = Paint().apply {
            isAntiAlias = true
            color = Color.WHITE
            style = Paint.Style.FILL
        }
        canvas?.drawCircle(x.toFloat(), y.toFloat(), r.toFloat(), paint)
    }

    override fun update(dt: Long) {
        r += dt*growthRate
        if(r > Resources.getSystem().displayMetrics.heightPixels) {
            Background.backgroundColor = Color.WHITE
            expired = true
        }
    }

}
