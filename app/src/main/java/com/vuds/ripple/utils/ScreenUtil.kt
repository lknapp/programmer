package com.vuds.ripple.utils

import android.content.res.Resources
import androidx.fragment.app.Fragment

class ScreenUtil : Fragment() {

    companion object {
        val navBarHeight = 80;

        fun centerX(): Double {
            return Resources.getSystem().getDisplayMetrics().widthPixels/2.0

        }

        fun randomPaddedX(padding: Double): Double {
            return (Math.random()* (Resources.getSystem().getDisplayMetrics().widthPixels - 2*padding)) + padding
        }

        fun randomPaddedY(padding: Double): Double {
            return (Math.random()*(Resources.getSystem().getDisplayMetrics().heightPixels - 2*padding - navBarHeight)) + padding
        }

        fun maxX(): Double {
            return (Resources.getSystem().getDisplayMetrics().widthPixels).toDouble()
        }

        fun maxY(): Double {
            return (Resources.getSystem().getDisplayMetrics().heightPixels - navBarHeight).toDouble()
        }
    }
}