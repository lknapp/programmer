package com.vuds.ripple.utils

import android.graphics.Color
import android.graphics.Color.alpha
import kotlin.random.Random

class ColorUtil {
    companion object {
        fun randomBrightColor(): Int {
            return Color.HSVToColor(255, listOf(Random.nextFloat() *360, Random.nextFloat() *0.25f+0.75f, Random.nextFloat() *0.25f+0.75f).toFloatArray())
        }

        fun randomLightColor(): Int {
            return Color.HSVToColor(255, listOf(Random.nextFloat()*360, 0.35f, 1f).toFloatArray())
        }

        fun rotateHue(c: Int): Int {
            val hsvValues = FloatArray(3)
            Color.colorToHSV(c, hsvValues)
            hsvValues[0] = (hsvValues[0] + 1)%360
            return Color.HSVToColor(alpha(c), hsvValues)
        }

        fun fade(c: Int): Int {
            var alpha = alpha(c) - 1
            alpha = if (alpha < 1) 0 else alpha
            val hsvValues = FloatArray(3)
            Color.colorToHSV(c, hsvValues)
            return Color.HSVToColor(alpha, hsvValues)
        }

        fun darken(c:  Int): Int {
            val hsvValues = FloatArray(3)
            Color.colorToHSV(c, hsvValues)
            hsvValues[2] = if (hsvValues[2] > 0) (hsvValues[2] - 0.002f) else 0f
            return Color.HSVToColor(255, hsvValues)
        }

    }
}